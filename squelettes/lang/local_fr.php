<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // Structure (!!! pas de " dans les phrases à traduire, les ' doivent être échapée: \' !!!):
    'ne_pas_changer' => 'Phrase à traduire',
    'ne_pas_changer_avec_apostrophe' => 'Phrase à traduire avec l\'apostrophe',
    
	// A
	'acceder_aux_hyperpaysages' => 'Accéder aux hyperpaysages',
    'afficher_cap' => 'Je regarde vers où ?',
	
	// C
	'concept_hyperpaysages' => 'Hyperpaysages est un concept imaginé et développé par l\'<a href="http://www.institut-eco-pedagogie.be/spip/">Institut d\'Eco-Pédagogie</a> et le <a href="http://www.lmg.ulg.ac.be/spip/">Laboratoire de Méthodologie de la Géographie de l\'ULg</a>.<br />Site web créé par <a href="http://conception.sebille.name">Robert Sebille</a>.',
    
    // E
    'est' => 'est',

    // I
    'inconnu' => 'inconnu',
    'infobulle_arreter_defilement_et_afficher_orientation' => 'Arrêter le défilement et afficher l\'orientation.',
    'infobulle_reprendre_defilement' => 'Redémarrer.',
    
    // M
    'moins_vite' => 'Moins vite',
    
    // N
    'nord' => 'nord',
    'nord_est' => 'nord est',
    'nord_ouest' => 'nord ouest',
                
    // O
    'ouest' => 'ouest',
    
    // P
    'pause' => 'Pause',
    'plus_vite' => 'Plus vite',
    
    // S
    'secteur' => 'secteur',
    'sud_est' => 'sud est',
    'sud' => 'sud',
    'sud_ouest' => 'sud ouest',
    
    // V
    'vers_la_droite' => 'Vers la droite',
    'vers_la_gauche' => 'Vers la gauche',
    'vitesse_maximum_atteinte' => 'Vitesse maximum atteinte.',
    'vitesse_minimum_atteinte' => 'Vitesse minimum atteinte.',
    
    // Laisser ci-dessous tel quel - Don't change below
	'pas_de_virgule_pour_les_distraits' => 'Pas de virgule pour les distraits'

);
