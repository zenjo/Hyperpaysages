<?php
// This is a SPIP language file  --  Cecieast un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // Structure (!!! pas de " dans les phrases à traduire, les ' doivent être échapée: \' !!!):
    'ne_pas_changer' => 'Phrase à traduire',
    'ne_pas_changer_avec_apostrophe' => 'Phrase à traduire avec l\'apostrophe',
    
	// A
	'acceder_aux_hyperpaysages' => 'Access to hyperlandscapes',
    'afficher_cap' => 'Compass heading ?',

	// C
	'concept_hyperpaysages' => 'Hyperlandscapes is a concept designed and developed by the <a href="http://www.institut-eco-pedagogie.be/spip/">Institut d\'Eco-Pédagogie</a> and the <a href="http://www.lmg.ulg.ac.be/spip/">Laboratoire de Méthodologie de la Géographie de l\'ULg</a>.<br />Website created by <a href="http://conception.sebille.name">Robert Sebille</a>.',
        
    // E
    'est' => 'east',

    // I
    'inconnu' => 'unknow',
    'infobulle_arreter_defilement_et_afficher_orientation' => 'Stop scrolling and display the compass heading.',
    'infobulle_reprendre_defilement' => 'Restart.',
    
    // M
    'moins_vite' => 'Slower',
    
    // N
    'nord' => 'north',
    'nord_est' => 'northeast',
    'nord_ouest' => 'northwest',
                
    // O
    'ouest' => 'west',
    
    // P
    'pause' => 'Pause',
    'plus_vite' => 'Faster',
    
    // S
    'secteur' => 'sector',
    'sud_est' => 'southeast',
    'sud' => 'south',
    'sud_ouest' => 'southwest',
    
    // V
    'vers_la_droite' => 'To the right',
    'vers_la_gauche' => 'To the left',
    'vitesse_maximum_atteinte' => 'Maximum speed reached.',
    'vitesse_minimum_atteinte' => 'Minimum speed reached.',
    
    // Laisser ci-dessous tel quel - Don't change below
	'pas_de_virgule_pour_les_distraits' => 'Pas de virgule pour les distraits'

);
