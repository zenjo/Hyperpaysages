<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // Structure (!!! pas de " dans les phrases à traduire, les ' doivent être échapée: \' !!!):
    'ne_pas_changer' => 'Phrase à traduire',
    'ne_pas_changer_avec_apostrophe' => 'Phrase à traduire avec l\'apostrophe',
    
	// A
	'acceder_aux_hyperpaysages' => 'Accesso a iperpaesaggi',
    'afficher_cap' => 'Direzione della bussola ?',
    
	// C
	'concept_hyperpaysages' => 'Iperpaesaggi è un concetto progettato e sviluppato dall\'<a href="http://www.institut-eco-pedagogie.be/spip/">Institut d\'Eco-Pédagogie</a> e il <a href="http://www.lmg.ulg.ac.be/spip/">Laboratoire de Méthodologie de la Géographie de l\'ULg</a>.<br />Sito realizzato da <a href="http://conception.sebille.name">Robert Sebille</a>.',

    // E
    'est' => 'est',

    // I
    'inconnu' => 'sconosciuto',
    'infobulle_arreter_defilement_et_afficher_orientation' => 'Interrompere lo scorrimento e visualizzare la direzione della bussola.',
    'infobulle_reprendre_defilement' => 'Ripartire.',
    
    // M
    'moins_vite' => 'Più lento',
    
    // N
    'nord' => 'a nord',
    'nord_est' => 'a nord-est',
    'nord_ouest' => 'a nord-ovest',
                
    // O
    'ouest' => 'a ovest',
    
    // P
    'pause' => 'Pausa',
    'plus_vite' => 'Più veloce',
    
    // S
    'secteur' => 'settore',
    'sud_est' => 'a sud-est',
    'sud' => 'a sud',
    'sud_ouest' => 'a sud-ovest',
    
    // V
    'vers_la_droite' => 'Andare a destra',
    'vers_la_gauche' => 'Andare a sinistra',
    'vitesse_maximum_atteinte' => 'Velocità massima.',
    'vitesse_minimum_atteinte' => 'Velocità minima.',
    
    // Laisser ci-dessous tel quel - Don't change below
	'pas_de_virgule_pour_les_distraits' => 'Pas de virgule pour les distraits'

);
