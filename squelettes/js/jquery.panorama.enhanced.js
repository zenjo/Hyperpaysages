/* =========================================================
// jquery.panorama.js
// Author: OpenStudio (Arnault PACHOT)
// Mail: apachot@openstudio.fr
// Web: http://www.openstudio.fr
// Copyright (c) 2008 Arnault Pachot
// licence : GPL
========================================================= */
/* jquery.panorama.enhanced.js
// Forked version from jQuery virtual tour, downloaded 03/08/2016 (3 august)
// from http://www.openstudio.fr/lab/jQuery-virtual-tour,67.html
// Author: Robert Sebille
// Company: IEP
// Copyright (c) 2016 - Licence GPL
// Version: 0.03 19/12/2016
========================================================= */

(function($) {
	$.fn.panorama = function(options) {
		this.each(function(){ 
			var settings = {
				viewport_width: 600,
				speed: 20000,
                speed_factor: 2,
                speed_min: 200000,
                speed_max: 2000,
                speed_alert: true,
                speed_min_alert_message: "Vitesse minimum atteinte.",
                speed_max_alert_message: "Vitesse maximum atteinte.",
				direction: 'left',
				control_display: 'auto',
				start_position: 0,
                north_pixel:0,
				auto_start: true,
				mode_360: true,
                show_cap: true,
                false_cap_display: "Je regarde vers où ?",
                false_cap_title: "Arrêter le défilement et afficher l'orientation.",
                true_cap_title: "Redémarrer.",
                // Translate
                sector: "secteur",
                north: "nord",
                northeast: "nord est",
                east: "est",
                southeast: "sud est",
                south: "sud",
                southwest: "sud ouest",
                west: "ouest",
                northwest: "nord ouest",
                unknow: "inconnu",
                //Controls tooltips
                slower: "Moins vite",
                to_the_left: "Vers la gauche",
                pause: "Pause",
                to_the_right: "Vers la droite",
                faster: "Plus vite"
			};
			if(options) $.extend(settings, options);
		
			var elemWidth = parseInt($(this).attr('width'));
			var elemHeight = parseInt($(this).attr('height'));
			var currentElement = this;
			var panoramaViewport, panoramaContainer;
			var bMouseMove = false;
			var mouseMoveStart = 0;
			var mouseMoveMarginStart = 0;

            var capDisplayed = false;
            if (settings.show_cap == true) {
                $("DIV#boussole, DIV#viseur").css('text-align', 'center').css('cursor', 'pointer').show();
                actualiserOrientation();
            } else {
				$("DIV#boussole").hide();
				$("DIV#viseur").show();
			}

			$(this).attr('unselectable','on')
				.css('position', 'relative')
				.css('-moz-user-select','none')
				.css('-webkit-user-select','none')
				.css('margin', '0')
				.css('padding', '0')
				.css('border', 'none')
				.wrap("<div class='panorama-container'></div>");
			if (settings.mode_360) 
				$(this).clone().insertAfter(this);
			
			panoramaContainer = $(this).parent();
			panoramaContainer.css('height', elemHeight+'px').css('overflow', 'hidden').wrap("<div class='panorama-viewport'></div>").parent().css('width',settings.viewport_width+'px')
				.append("<div class='panorama-control'><a href='#' class='panorama-control-moins' title='"+settings.slower+"'>--</a> <a href='#' class='panorama-control-left' title='"+settings.to_the_left+"'><<</a> <a href='#' class='panorama-control-pause' title='"+settings.pause+"'>x</a> <a href='#' class='panorama-control-right' title='"+settings.to_the_right+"'>>></a> <a href='#' class='panorama-control-plus' title='"+settings.faster+"'>++</a> </div>");
			
			panoramaViewport = panoramaContainer.parent();

			panoramaViewport.mousedown(function(e){
			      if (!bMouseMove) {
				bMouseMove = true;
				mouseMoveStart = e.clientX;
			      }
			      return false;
			}).mouseup(function(){
			      bMouseMove = false;
			      mouseMoveStart = 0;
			      return false;
			}).mousemove(function(e){
			      if (bMouseMove){
				  var delta = parseInt((mouseMoveStart - e.clientX)/30);
				  if ((delta>10) || (delta<10)) {
				      var newMarginLeft = parseInt(panoramaContainer.css('marginLeft')) + (delta);
				      if (settings.mode_360) {
					    if (newMarginLeft > 0) {newMarginLeft = -elemWidth;}
					    if (newMarginLeft < -elemWidth) {newMarginLeft = 0;}
				      } else {
					    if (newMarginLeft > 0) {newMarginLeft = 0;}
					    if (newMarginLeft < -elemWidth) {newMarginLeft = -elemWidth;}
				      }
				      panoramaContainer.css('marginLeft', newMarginLeft+'px');
				  }
				
			      }
			}).bind('contextmenu',function(){return false;});
			
			panoramaViewport.css('height', elemHeight+'px').css('overflow', 'hidden').find('a.panorama-control-left').bind('click', function() {
				$(panoramaContainer).stop();
				settings.direction = 'right';
				panorama_animate(panoramaContainer, elemWidth, settings);
                if (settings.show_cap == true) {actualiserOrientation();}
				return false;
			});
			panoramaViewport.bind('click', function() {
				$(panoramaContainer).stop();
			});
			panoramaViewport.find('a.panorama-control-right').bind('click', function() {
				$(panoramaContainer).stop();
				settings.direction = 'left';
				panorama_animate(panoramaContainer, elemWidth, settings);
                if (settings.show_cap == true) {actualiserOrientation();}
				return false;
			});
			panoramaViewport.find('a.panorama-control-pause').bind('click', function() {
				$(panoramaContainer).stop();
				return false;
			});
			panoramaViewport.find('a.panorama-control-moins').bind('click', function() {
                var new_speed = Math.round(settings.speed * settings.speed_factor);
				$(panoramaContainer).stop();
				if (new_speed <= settings.speed_min) {settings.speed = new_speed;} 
                    else {settings.speed = settings.speed_min; if (settings.speed_alert) {alert(settings.speed_min_alert_message);}}
				panorama_animate(panoramaContainer, elemWidth, settings);
                if (settings.show_cap == true) {actualiserOrientation();}
				return false;
			});			
			panoramaViewport.find('a.panorama-control-plus').bind('click', function() {
                var new_speed = Math.round(settings.speed / settings.speed_factor);
				$(panoramaContainer).stop();
				if (new_speed >= settings.speed_max) {settings.speed = new_speed;} 
                    else {settings.speed = settings.speed_max; if (settings.speed_alert) {alert(settings.speed_max_alert_message);}}
				panorama_animate(panoramaContainer, elemWidth, settings);
                if (settings.show_cap == true) {actualiserOrientation();}
				return false;
			});			
			
			if (settings.control_display == 'yes') {
				panoramaViewport.find('.panorama-control').show();
			} else if (settings.control_display == 'auto') {
				panoramaViewport.bind('mouseover', function(){
					$(this).find('.panorama-control').show();
					return false;
				}).bind('mouseout', function(){
					$(this).find('.panorama-control').hide();
					return false;
				});
				
			}
		
			$(this).parent().css('margin-left', '-'+settings.start_position+'px');

			if (settings.auto_start) 
				panorama_animate(panoramaContainer, elemWidth, settings);

            $("DIV#boussole, DIV#viseur").bind('click', function(){
                if (capDisplayed == false) {
    				$(panoramaContainer).stop();
                    currentMarginLeft = 0-parseInt($("DIV.panorama-container").css('margin-left'));
                    currentMiddle = parseInt(currentMarginLeft + (settings.viewport_width / 2));
                    orientationDegre = 0;
                    if (settings.north_pixel < currentMiddle) {orientationDegre = parseInt((currentMiddle - settings.north_pixel) / elemWidth * 360);}
                    if (settings.north_pixel > currentMiddle) {orientationDegre = parseInt((elemWidth - settings.north_pixel + currentMiddle) / elemWidth * 360);}
                    secteur = Math.round(orientationDegre / 45);
                    if (secteur > 7) {secteur = 0;}
                    switch(secteur) {
                       case 0:
                            nomSecteur = settings.north;
                            break;
                        case 1:
                            nomSecteur = settings.northeast;
                            break;
                        case 2:
                            nomSecteur = settings.east;
                            break;
                        case 3:
                            nomSecteur = settings.southeast;
                            break;
                        case 4:
                            nomSecteur = settings.south;
                            break;
                        case 5:
                            nomSecteur = settings.southwest;
                            break;
                        case 6:
                            nomSecteur = settings.west;
                            break;
                        case 7:
                            nomSecteur = settings.northwest;
                            break;
                        default:
                            nomSecteur = settings.unknown;
                        } 
    		    	$("DIV#boussole").attr('data-title', settings.true_cap_title).html(orientationDegre+"°, "+settings.sector+" "+nomSecteur);
    		    	//$("DIV#viseur").attr('data-title', settings.true_cap_title);
                    capDisplayed = true;
                } else {
                    panorama_animate(panoramaContainer, elemWidth, settings);
                    actualiserOrientation();
                    }
				return false;
            });

            function actualiserOrientation(){
                $("DIV#boussole").attr('data-title', settings.false_cap_title).html(settings.false_cap_display);
    		    //$("DIV#viseur").attr('data-title', settings.false_cap_title);
                capDisplayed = false;
                return false;
            }

		});
		function panorama_animate(element, elemWidth, settings) {
			currentPosition = 0-parseInt($(element).css('margin-left'));
			//$("span#orientation").text(currentPosition);

			if (settings.direction == 'right') {
				
				$(element).animate({marginLeft: 0}, ((settings.speed / elemWidth) * (currentPosition)) , 'linear', function (){ 
					if (settings.mode_360) {
						$(element).css('marginLeft', '-'+(parseInt(parseInt(elemWidth))+'px'));
						panorama_animate(element, elemWidth, settings);
					}
				});
			} else {
 				
				var rightlimit;
				if (settings.mode_360) 
					rightlimit = elemWidth;
				else
					rightlimit = elemWidth-settings.viewport_width;
					
				$(element).animate({marginLeft: -rightlimit}, ((settings.speed / rightlimit) * (rightlimit - currentPosition)), 'linear', function (){ 
				//$(element).animate({marginLeft: -rightlimit},  {duration: 10000, step: function(currentPosition){$("span#orientation").text("hello"+currentPosition);}}, ((settings.speed / rightlimit) * (rightlimit - currentPosition)), 'linear',  function (){ 
					if (settings.mode_360) {
						$(element).css('margin-left', 0); 
						panorama_animate(element, elemWidth, settings);
					}
				});
			}

			
		}
		
	};

$(document).ready(function(){
	$("img.panorama").panorama();
});
})(jQuery);
