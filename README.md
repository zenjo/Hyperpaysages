# Concept
Référez-vous à [cet article](https://gitlab.adullact.net/zenjo/Hyperpaysages/wikis/Home).

# Documentation
* [Installation](https://hyperpaysage.be/modele/dynamic/spip.php?article38)
* [Réalisation et publication](https://hyperpaysage.be/modele/dynamic/spip.php?rubrique4) d'un hyperpaysage.
* [Règles de configuration](https://gitlab.adullact.net/zenjo/Hyperpaysages/blob/master/LISEZMOI_CONFIGURATION_PANORAMA) propres à jquery.panorama.enhanced.js. 

__Il est déconseillé de modifier directement  dans le code les règles de configuration propres à jquery.panorama.enhanced.js.__ Il vaut mieux se référer à l'article "[Les règles de publication propres au modèle Hyperpaysages](https://hyperpaysage.be/modele/dynamic/spip.php?article22)". Si, cependant, vous vous y connaissez et que vous voulez quand même intervenir sur la configuration brute, alors, c'est dans [ce squelette](https://gitlab.adullact.net/zenjo/Hyperpaysages/blob/master/squelettes/inclure/head.html)

# Utilitaire
Placez le répertoire _utils_ à la racine de votre site, un utilitaire sera alors disponible à l'url _http://<adresse_de_votre_site\>/utils_. Cet utilitaire vous permettra de générer automatiquement le code _SPIP_ du panoramique à partir de la carte cliquable que vous aurez généré via _[Gimp](https://www.gimp.org/)_.

[Plus d'informations à ce propos](https://hyperpaysage.be/modele/dynamic/spip.php?article22#outil_sommaire_3).

# Traductions
Elles concernent les traductions pour _jquery.panorama.enhanced.js_; (_SPIP_ est déjà traduit dans de multiples langues). Elles sont dans ce [répertoire](https://gitlab.adullact.net/zenjo/Hyperpaysages/tree/master/squelettes/lang), fichiers _local_xx.php_, où _xx_ = l'identificateur de la langue au format _[ISO 3166-2](https://fr.wikipedia.org/wiki/ISO_3166-2)_. 

Pour traduire, copiez un _local_xx.php_ de votre choix vers un _local_yy.php_, où _yy_ est le code _ISO 3166-2_ de la langue dans laquelle vous allez traduire.

# Exemples
* [OC3 Geografia LIME](http://www.iperpaesaggi.ch/spip.php?article181)
* [3D Liceo cantonale di Bellinzona ](http://www.iperpaesaggi.ch/spip.php?article131)
