<?php
function init(){
GLOBAL $name_pano, $title_pano, $gimp_code, $hp_code;
	$name_pano = ""; $title_pano = ""; $gimp_code = "";
	$hp_code = "<p>Fill the form below, click on the \"Build Spip code\" button and the SPIP hyperlandscape image map code will be coming here. Then copy / paste it in your spip article.</p>";
}
init();

if ($_GET['file'] == "g2s" or $_GET['file'] == "g2sf") {
	if ($_GET['file'] == "g2s") {highlight_file("gimp2spip.php");}
	if ($_GET['file'] == "g2sf") {highlight_file("gimp2spip_functions.php");}
} else {
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Transform a gimp image map code to spip hyperlandscape image map code.</title>

	<style  type="text/css">
		BODY {background-color: #FFFDCE;}
        #page h1 {font-size: 100%;}
		#page h2 {font-size: 95%;}
        #page DIV#entete, #page DIV#pied, #page DIV#dessus, #page DIV#dessous, #page DIV#centre {background-color: #FFF; width: 850px; text-align: justify; padding: 5px; margin:auto; margin-bottom: 10px; box-shadow: 5px 7px 5px 2px rgba(85, 85, 85, 0.3); border-radius: 0.50em 0.50em 0.50em 0.50em;}
        #page DIV#entete *, #page DIV#pied * {text-align: center;}
        #page DIV#entete {border-bottom: 1px solid #3F0E63; text-align: center;}
        #page DIV#pied {border-top: 1px solid #3F0E63; text-align: center;}
        #page DIV#dessus, #page DIV#dessous, #page DIV#centre {border: 1px solid #3F0E63;}
        #page P {text-align: justify;}
		#page {
			text-align: center;
		}

    </style>
</head>
<body>
	
<?php
include ('gimp2spip_functions.php');
if (isset($_POST['form_reset'])) {
	init();
	}
	
if (isset($_POST['build_spip_code'])) {
	if (isset($_POST['name_pano'])) {$name_pano = $_POST['name_pano'];}
	if (isset($_POST['title_pano'])) {$title_pano = $_POST['title_pano'];}
	if (isset($_POST['gimp_code'])) {$gimp_code = $_POST['gimp_code'];}
	if (isset($_POST['hp_code'])) {$hp_code = $_POST['hp_code'];}
	$hp_code = gimp2spip ($gimp_code, $name_pano, $title_pano);
	}

?>

<div id="page">

    <div id="entete">
		<p style="font-size: 80%;">License GNU Gpl. V. 0.2. Source code&nbsp;:&nbsp;<a href="?file=g2s">gimp2spip</a>&nbsp;-&nbsp;<a href="?file=g2sf">gimp2spip_functions</a></p>
		<p style="font-size: 80%;">Transform a gimp image map code to an hyperlandscape image map code for SPIP using IEP templates with jquery.panorama.enhanced.js.</p>
		<p style="font-size: 80%; font-weight: bold;">Play attention, your gimp image map code must be compliant with <a href="https://codingteam.net/project/hyperpaysages/download">IEP SPIP templates</a> !</p>
    </div>
	
    <h1>Hyperlandscape image map code generator for SPIP.</h1>

    <div id="dessus">
		<?php echo $hp_code; ?>

    </div>
 
    <div id="centre">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" name="input">
			<input type="text" name="name_pano" value="<?php echo $name_pano; ?>" /> Panoramic file name <span style="font-size: 80%;">(required)</span>.<br />
			<input type="text" name="title_pano" value="<?php echo $title_pano; ?>" /> Panoramic title <span style="font-size: 80%;">(not required, max 20 chars)</span>.<br />
			Gimp image map code <span style="font-size: 80%;">(required)</span> :<br />
			<textarea name="gimp_code" cols="80" rows="15"><?php echo $gimp_code; ?></textarea><br />
			<input type="submit" name="build_spip_code" value="Build Spip code." />&nbsp;-&nbsp;
			<input type="submit" name="form_reset" value="Form reset." />
			
		</form>
    </div>

	<div id="dessous">
		<p>Panoramic file name is the name of the file of the panoramic you uploaded with SPIP, without the path "IMG/&lt;type&gt;/". Example below : "bois_de_la_cambre1.jpg"</p>
		<p style="text-align: center;"><img src="nom_image.jpg" width="197" height="147" alt="Panoramic file name image" /></p>

	</div>

    <div id="pied">
		<p style="text-align: center; font-size: 80%;">&copy; Institut d'eco-pédagogie, Liège, Belgium.</p>
    </div>

</div>

</body>
</html>
<?php
}
?>
