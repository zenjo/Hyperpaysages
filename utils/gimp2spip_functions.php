<?php

function gen_map_name() {
GLOBAL $map_name;
    $map_name = "";
    for ($i=0; $i<4; $i++) {$map_name .= chr(mt_rand(65,90));}
    $map_name .= "_map";
    return $map_name;
}

function add_tag($tag, $ligne) {
    if (strpos($ligne, " />") !== false) {$ligne = str_replace(" />", " ".$tag." />", $ligne);}
        else {$ligne = str_replace(">", " ".$tag.">", $ligne);}
    return $ligne;
}

function change_tag($tag, $valeur, $ligne) {
    //return preg_replace("/".$tag."=\"(\S*)\"/", "${1}".$tag."=\"".$valeur."\"", $ligne);
    return preg_replace("/".$tag."=\"((\S|\s)*)\"/U", "\"".$tag."=\"".$valeur."\"", $ligne);
}

function process_document_line($ligne, $name_pano, $title_pano) {
GLOBAL $map_name;
    $ligne = change_tag("map", $map_name, $ligne);
    $ligne = add_tag("class=\"advancedpanorama\"", $ligne);
    if (strlen($title_pano) > 20) {$title_pano = substr($title_pano, 0, 20);}
    $ligne = add_tag("alt=\"$title_pano\"", $ligne);
    $extension = substr(strrchr($name_pano, "."), 1);
    $name_pano = "IMG/".$extension."/".$name_pano;
    $ligne = change_tag("src", $name_pano, $ligne);
    
    return $ligne;
}

function process_map_line($ligne) {
GLOBAL $map_name;
    $ligne = change_tag("name", "map", $ligne);
    $ligne = str_replace("name=", "id=", $ligne);
    $ligne = add_tag("name=\"".$map_name."\"", $ligne);
    return $ligne;    
}

function process_area_line($ligne) {
    if (strpos($ligne, ".jpg") !== false or strpos($ligne, ".png") !== false or strpos($ligne, ".gif") !== false) {
        $ligne = add_tag("class=\"mediabox\"", $ligne);
    }
    if (strpos($ligne, "alt=") === false) {$ligne = add_tag("alt=\"\"", $ligne);}
    return $ligne;
}

function clean_array($tableau, $name_pano, $title_pano) {
    $prov = array();
    foreach($tableau as $val) {
        //array_push($prov, $val);
        if (strpos($val, "<img") !== false) {$val = process_document_line($val, $name_pano, $title_pano); array_push($prov, $val);}
        if (strpos($val, "<map") !== false) {$val = process_map_line($val); array_push($prov, $val);}
        if (strpos($val, "<area") !== false) {$val = process_area_line($val); array_push($prov, $val);}
        if (strpos($val, "</map") !== false) {array_push($prov, $val);}
    }
    return $prov;
}

function add_code_before_after ($code) {
    $before = "<p style=\"font-size: 80%;\">Code to copy / paste in your SPIP article :</p>";
    $before .= "<form action=\"\" method=\"POST\" name=\"output\">\n<textarea cols=\"80\" rows=\"15\" onclick=\"this.select();\" readonly=\"readonly\">";
    $before .= "<html><div id=\"panoramique\">\n<div id=\"boussole\"></div>\n<div id=\"viseur\">{°|°}</div>\n\n";
    $after = "\n\n</div></html></textarea>\n</form>\n";
    return $before.$code.$after;
}

function gimp2spip ($code, $name_pano, $title_pano) {
GLOBAL $map_name;

    if ($code == "" or $name_pano == "") {
        $prov = "";
        if ($name_pano == "") {$prov = "It seems you forget the panoramic file name. Panoramic file name is the name of the file of the panoramic you uploaded with SPIP, without the path \"IMG/&lt;type&gt;/\". See the example at the bottom of the page.";}
        if ($code == "") {$prov = "It seems you forget the gimp image map code ;).";}
        $code = $prov;
    } else {
        $map_name = gen_map_name();
        $code_array = array();
        $code_array = preg_split("/[\n]+/", $code);
        $code_array = clean_array($code_array, $name_pano, $title_pano);
//print_r($code_array);
//die();
        $code = implode("\n", $code_array);
        $code = add_code_before_after ($code);
    }
    
    return $code;
}

?>
